from django.shortcuts import render

# Create your views here.
from django.urls import path, reverse
from django.views.generic import ListView, CreateView, UpdateView, DetailView

from wantedapp.forms import WantedCreationForm
from wantedapp.models import Wanted


class WantedCreateView(CreateView):
    model = Wanted
    form_class = WantedCreationForm
    template_name = 'wantedapp/create.html'

    def get_success_url(self):
        return reverse('wantedapp:list')

# class WantedUpdateView(UpdateView):
# class WantedDetailView(DetailView):

class WantedListView(ListView):
    model = Wanted
    context_object_name = 'wanted_list'
    template_name = 'wantedapp/list.html'

class WantedDetailView(DetailView):
    model = Wanted
    context_object_name = 'target_wanted'
    template_name = 'wantedapp/detail.html'

class WantedUpdateView(UpdateView):
    model = Wanted
    form_class = WantedCreationForm
    context_object_name = 'target_wanted'
    template_name = 'wantedapp/update.html'

    def get_success_url(self):
        return reverse('wantedapp:detail', kwargs={'pk': self.object.pk})