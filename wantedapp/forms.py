from django.forms import ModelForm, forms, DateInput, DateField

from wantedapp.models import Wanted


class WantedCreationForm(ModelForm):
    end_date = DateField(widget=DateInput(format='%d/%m/%Y'), input_formats=('%d/%m/%Y',))

    class Meta:
        model = Wanted
        fields = ['image','job','company','country','work_place','address','end_date','compensation','content']