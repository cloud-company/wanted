from django.apps import AppConfig


class WantedListConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'wantedapp'
