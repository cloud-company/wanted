from django.db import models

# Create your models here.

class Wanted(models.Model):
    image = models.ImageField(upload_to='wanted/', null=False)

    # Foregin Key
    job = models.CharField(max_length=200)
    company = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    work_place = models.CharField(max_length=200, null=True)
    created_at = models.DateField(auto_now_add=True, null=True)
    end_date = models.DateField(auto_now=False, auto_now_add=False, null=True)
    address = models.CharField(max_length=200, null=True)

    like = models.IntegerField(null=True)
    compensation = models.IntegerField(null=True)
    content = models.TextField(null=True)