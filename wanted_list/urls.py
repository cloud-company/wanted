from django.urls import path

from wanted_list.views import WantedListView

app_name = 'wdlist'

urlpatterns = [
    path('list/', WantedListView.as_view(), name='list')
]