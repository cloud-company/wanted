from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView

from wanted_list.models import test


class WantedListView(ListView):
    model = test
    context_object_name = 'wantedapp'
    template_name = 'wantedapp/list.html'
    paginate_by = 5