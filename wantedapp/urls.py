from django.urls import path, reverse

from wantedapp.views import WantedListView, WantedCreateView, WantedDetailView, WantedUpdateView

app_name = "wantedapp"

urlpatterns = [
    # path('home/', reverse('wantedapp:home'), name='home'),
    path('create/', WantedCreateView.as_view(), name='create'),
    path('detail/<int:pk>', WantedDetailView.as_view(), name='detail'),
    path('update/<int:pk>', WantedUpdateView.as_view(), name='update'),
    path('list/', WantedListView.as_view(), name='list'),
]